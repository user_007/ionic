import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { IHotel } from '../hotels/hotels';

@IonicPage()
@Component({
  selector: 'page-hotel-details',
  templateUrl: 'details.html',
})
export class HotelDetailsPage {
  hotel: IHotel;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.hotel = navParams.get('details');
  }
}
