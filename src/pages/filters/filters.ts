import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';

export interface IFilters {
  minPrice?: number;
  maxPrice?: number;
  hasParking: boolean;
}

type errors = {
  minPrice?: string;
  maxPrice?: string;
};

@IonicPage()
@Component({
  selector: 'page-filters',
  templateUrl: 'filters.html',
})
export class FiltersPage {
  filters: IFilters;
  validation: {
    isValid: boolean;
    errors?: errors;
  };

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    private _storage: Storage,
    private _event: Events,
    public navParams: NavParams,
  ) {
    this.filters = {
      maxPrice: 0,
      minPrice: 0,
      hasParking: false,
    };

    this.validation = { isValid: true, errors: {} };
  }

  async ionViewDidLoad() {
    const filtersFromStorage = await this._storage.get('filters');

    if (filtersFromStorage) this.filters = { ...filtersFromStorage };
  }

  resetFilters() {
    this.filters = {
      maxPrice: 0,
      minPrice: 0,
      hasParking: false,
    };

    this.handleSubmitFilters();
  }

  closeFilters() {
    this.viewCtrl.dismiss();
  }

  async handleSubmitFilters() {
    await this._storage.set('filters', { ...this.filters });

    this._event.publish('filtersChange');
    this.viewCtrl.dismiss();
  }

  checkValidation() {
    this.validation.errors = {};
    this.validation.isValid = true;

    const minPrice = +this.filters.minPrice;
    const maxPrice = +this.filters.maxPrice;

    if (!Number.isNaN(minPrice) && minPrice < 0) {
      this.validation.errors.minPrice = 'значение меньше 0';
      this.validation.isValid = false;
    }

    if (!Number.isNaN(maxPrice) && maxPrice < 0) {
      this.validation.errors.maxPrice = 'значение меньше 0';
      this.validation.isValid = false;
    }
    if (maxPrice > 0 && minPrice > 0 && minPrice > maxPrice) {
      this.validation.errors.minPrice = `максимум ${maxPrice}`;
      this.validation.errors.maxPrice = `минимум ${minPrice}`;
      this.validation.isValid = false;
    }
  }
}
