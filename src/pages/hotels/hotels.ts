import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Events, ModalController, NavController } from 'ionic-angular';
import { HotelDetailsPage } from '../details/details';
import { FiltersPage, IFilters } from '../filters/filters';
import { hotelsData } from '../../mock-data';

export interface IHotel {
  imageUrl: string;
  title: string;
  description: string;
  roomCost: number;
  hasParking: boolean;
  address: string;
  phone: string;
}

type HotelsList = Array<{
  imageUrl: string;
  title: string;
  description: string;
  roomCost: number;
  hasParking: boolean;
  address: string;
  phone: string;
}>;

@Component({
  selector: 'page-hotels',
  templateUrl: 'hotels.html',
})
export class HotelsPage {
  hotels: HotelsList = [];
  _hotelsRawData: HotelsList = [];

  constructor(
    public navCtrl: NavController,
    public modCtrl: ModalController,
    private _storage: Storage,
    private _event: Events,
  ) {
    this._event.subscribe('filtersChange', () => {
      this.getHotelsList();
    });
  }

  async getHotelsList() {
    this._hotelsRawData = hotelsData;

    const filters: IFilters = await this._storage.get('filters');

    if (filters) {
      const { minPrice, maxPrice, hasParking } = filters;

      this.hotels = this._hotelsRawData.filter(hotel => {
        if (minPrice && hotel.roomCost < minPrice) return false;

        if (maxPrice && hotel.roomCost > maxPrice) return false;

        if (hasParking) return hotel.hasParking;

        return true;
      });
    } else {
      this.hotels = [...this._hotelsRawData];
    }
  }

  ionViewDidEnter() {
    this.getHotelsList();
  }

  showHotelDetails(hotel: IHotel) {
    this.navCtrl.push(HotelDetailsPage, { details: { ...hotel } });
  }

  presentFiltersModal() {
    const filtersModal = this.modCtrl.create(FiltersPage);
    filtersModal.present();
  }
}
